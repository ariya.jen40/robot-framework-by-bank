*** Settings *** 
Resource        resourcefile_home.robot
Library         SeleniumLibrary 
# Test Teardown   Close Browser

*** Variables *** 
*** Test Cases *** 

CIR>HUMANSERVICE
    GoToWeb
    Click Element                   ${Humanservice}  
    Page Should Contain Element     xpath://*[@class="col-12 mb-5 text-center"]
CIR>FOROFFICER
    GoToWeb
    Click Element                   ${forofficer}
    Select Window                   url=http://192.168.36.111/cirweb/#/account/login
    Page Should Contain             เข้าสู่ระบบ 
    Page Should Contain             คนหาย คนนิรนาม และศพนิรนาม
CIR>ReportMissing
    GoToWeb
    Click Element                   ${Reportmissing}
    Select Window                   url=http://192.168.36.111/cirweb/#/account/login
    Page Should Contain             เข้าสู่ระบบ 
    Page Should Contain             คนหาย คนนิรนาม และศพนิรนาม

CIR>UNKNOWBODY
    GoToWeb
    Click Element                   ${Unknowbody}
    CheckUnknowBodyDetail
CIR>Viewmoremissing
    GoToWeb
    Click Element                   ${Moreinformation}
    CheckMissingDetail
CIR>ViewMissingDetail
    GoToWeb
    Wait Until Element Is Visible   //body/cifs-app/cifs-home/div[@class='home']/cifs-home-announcement/div[@class='container announcement mt-5']/div[@class='announcement-list row mt-2 justify-content-center']/div[1]/cifs-home-announcement-item[1]/div[1]/img[1]
    Click Element                   //body/cifs-app/cifs-home/div[@class='home']/cifs-home-announcement/div[@class='container announcement mt-5']/div[@class='announcement-list row mt-2 justify-content-center']/div[1]/cifs-home-announcement-item[1]/div[1]/img[1]
    MissingDetail
CIR>Searchmissing>Detail
    Implicit Wait
    GoToWeb
    Click Element                   ${Searchmissing}
    TextandClickSearch
    Wait Until Element Is Visible   ${Firstrow}
    Click Element                   ${Firstrow}
CIR>Searchmissing>Report
    Implicit Wait
    GoToWeb
    Click Element                   ${Searchmissing}
    TextandClickSearch
    Wait Until Element Is Visible   ${reportMissingPerson} 
    Click Element                   ${reportMissingPerson}
    Select Window                   url=http://192.168.36.111/cirweb/#/account/login
    Page Should Contain             เข้าสู่ระบบ 
    Page Should Contain             คนหาย คนนิรนาม และศพนิรนาม

CIR>ViewStat
    Implicit Wait
    GoToWeb
    Click Element                   ${ViewStat}
    WaitandcheckText                ม.ค. 2562 - ธ.ค. 2562
    Click Element                   id:stat-showstat
    WaitandcheckText                ม.ค. 2563 - ธ.ค. 2563
    Select From List by Label       id:stat-typestat  รายเดือน
    # Select From List by Label       //body/cifs-app/cifs-stat/div[@class='container stat mb-5']/div[@class='row mb-3 dont-print']/div[@class='col-auto']/cifs-date-range-picker/div[@class='date-range-picker']/cifs-date-picker/form[@class='datepicker-form ng-touched ng-dirty ng-invalid']/div[@class='date-picker']/div[@class='date-picker__section month']/ng-select[@class='ng-select bottom searchable ng-single ng-touched error ng-dirty ng-invalid']/div[@class='ng-select-dropdown-outer bottom']/virtual-scroll[@class='ng-select-dropdown']/div[1]  มกราคม
# CMS
    # Open Browser    http://192.168.36.111/cms/#/    Chrome
    # Click Element       //div[contains(text(),'2')]
    # Click Element       //div[contains(text(),'3')]
    # Click Element       //div[contains(text(),'4')]
    # Click Element       //div[contains(text(),'5')]
    # Click Element       //div[contains(text(),'6')]
    # Click Element       //div[contains(text(),'7')]
    # Click Element   //li[4]//a[1]
    # Click Element   //li[5]//a[1]
    # Click Element   //li[6]//a[1]
    # Click Element   //li[7]//a[1]





