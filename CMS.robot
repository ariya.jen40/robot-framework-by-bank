*** Settings *** 
Resource        resourcefile_cms.robot
Resource        resourcefile_home.robot
Library         SeleniumLibrary 
Library         ImapLibrary2
Test Teardown   Close Browser

*** Variables *** 
*** Test Cases *** 

TC-CMSCIR-1001-01
    login valid

TC-CMSCIR-1001-02.1
    openbrowserandcheckpage
    Input Text          ${email_locater}    X
    Element Should Be Disabled  ${login_button}

TC-CMSCIR-1001-02.2
    openbrowserandcheckpage
    Input Text          ${password_locater}    X
    Element Should Be Disabled  ${login_button}

TC-CMSCIR-1001-02.3
    openbrowserandcheckpage
    Element Should Be Disabled  ${login_button}

TC-CMSCIR-1001-02.4
    openbrowserandcheckpage
    Input Credentials   @{email}[1]         @{password}[1]
    Click Element       ${login_button}
    WaitandcheckText    บัญชีของท่านไม่มีสิทธิเข้าใช้งาน กรุณาติดต่อผู้ดูแลระบบ

TC-CMSCIR-1001-02.5
    openbrowserandcheckpage
    Input Text          ${email_locater}        X
    Press Keys          ${email_locater}        BACKSPACE
    WaitandcheckText    กรุณากรอกชื่อผู้เข้าใช้
    Input Text          ${password_locater}     X
    Press Keys          ${password_locater}        BACKSPACE
    WaitandcheckText    กรุณากรอกรหัสผ่าน

TC-CMSCIR-1001-03
    login valid
    Click Element       ${logout}

TC-CMSCIR-1001-04  #ยังไม่เสร็จ
    openbrowserandcheckpage
    Click Element       ${forgot_password}
    WaitandcheckText    ลืมรหัสผ่าน
    WaitandcheckText    เพื่อความปลอดภัยกรุณาระบุ "อีเมล์" เพื่อรับรหัสผ่านในการกำหนดรหัสผ่านใหม่
    Input Text          ${emailF_locator}       @{email}[1]
    Click Element       ${forgot_button}
    WaitandcheckText    กรอกรหัสเพื่อยืนยันตัวตน
    WaitandcheckText    กรุณาใส่ OTP ที่ได้รับทาง ข้อความ หรือ อีเมล์ เพื่อเข้าสู่ระบบ รหัสผ่านของคุณมีอายุการใช้งาน 15 นาที
    Email Verification

TC-CMSCIR-1001-05  
    openbrowserandcheckpage
    Click Element       ${forgot_password}
    WaitandcheckText    ลืมรหัสผ่าน
    WaitandcheckText    เพื่อความปลอดภัยกรุณาระบุ "อีเมล์" เพื่อรับรหัสผ่านในการกำหนดรหัสผ่านใหม่
    Input Text          ${emailF_locator}       Bank@Gmail.com
    Click Element       ${forgot_button}
    WaitandcheckText    อีเมล์ไม่ถูกต้อง

TC-CMSCIR-2001-01.1
    login valid
    Click Element                       ${download}
    WaitandcheckText                    ดาวน์โหลด
    Click Element                       ${downloadAdd}
    WaitandcheckText                    เพิ่มเอกสารดาวน์โหลด        
    Choose File                         ${uploadfile}       C:/Users/Dell/Desktop/Sample/docx_2.docx
    Input Text                          ${filename}         ทดสอบไม่เผยแพร่
    Unselect Checkbox                   xpath://*[@id="gridCheck1"]
    Checkbox Should Not Be Selected     xpath://*[@id="gridCheck1"]
    Click Element                       ${Addnewdownload}
    WaitandcheckText                    ทดสอบไม่เผยแพร่
    Go To                               http://192.168.36.111/cir/#/download
    Click Element                       ${Clicktowebsite}
    Wait Until Page Does Not Contain    ทดสอบไม่เผยแพร่       timeout=2.0
    
TC-CMSCIR-2001-01.2
    login valid
    Click Element                       ${download}
    WaitandcheckText                    ดาวน์โหลด
    Click Element                       ${downloadAdd}
    WaitandcheckText                    เพิ่มเอกสารดาวน์โหลด        
    Choose File                         ${uploadfile}       C:/Users/Dell/Desktop/Sample/docx_2.docx
    Input Text                          ${filename}         ทดสอบเผยแพร่
    Checkbox Should Be Selected         xpath://*[@id="gridCheck1"]
    Click Element                       ${Addnewdownload}
    WaitandcheckText                    ทดสอบเผยแพร่
    Go To                               http://192.168.36.111/cir/#/download
    Click Element                       ${Clicktowebsite}
    Wait Until Page Contains            ทดสอบเผยแพร่       timeout=2.0

TC-CMSCIR-2001-02.1
    login valid
    Click Element                       ${download}
    WaitandcheckText                    ดาวน์โหลด
    Click Element                       ${downloadAdd}
    WaitandcheckText                    เพิ่มเอกสารดาวน์โหลด 
    Input Text                          ${filename}         ทดสอบสร้างผิด1
    Click Element                       ${Addnewdownload}
    WaitandcheckText                    กรุณาเลือกไฟล์ที่ต้องการอัพโหลด

TC-CMSCIR-2001-02.2
    login valid
    Click Element                       ${download}
    WaitandcheckText                    ดาวน์โหลด
    Click Element                       ${downloadAdd}
    WaitandcheckText                    เพิ่มเอกสารดาวน์โหลด 
    Choose File                         ${uploadfile}       C:/Users/Dell/Desktop/Sample/docx_2.docx
    Click Element                       ${Addnewdownload}
    WaitandcheckText                    กรุณาระบุชื่อเอกสาร

TC-CMSCIR-2001-02.3
    login valid
    Click Element                       ${download}
    WaitandcheckText                    ดาวน์โหลด
    Click Element                       ${downloadAdd}
    WaitandcheckText                    เพิ่มเอกสารดาวน์โหลด 
    Click Element                       ${Addnewdownload}
    WaitandcheckText                    กรุณาเลือกไฟล์ที่ต้องการอัพโหลด

TC-CMSCIR-2001-03 
    login valid
    Click Element                       ${download}
    WaitandcheckText                    ดาวน์โหลด
    Click Element                       xpath:/html/body/cifs-root/div/cifs-download/div/div[2]/cifs-download-list/div/div[2]/div/table/tbody/tr[5]/td[5]/a[1]/i
    Input Text                          ${filename}     แก้ไข
    Click Element                       ${Editdownload}
    WaitandcheckText                    วันที่มีผลจะต้องไม่น้อยกว่าวันปัจจุบัน

TC-CMSCIR-2001-04
    login valid
    Click Element                       ${download}
    WaitandcheckText                    ดาวน์โหลด
    Click Element                       xpath:/html/body/cifs-root/div/cifs-download/div/div[2]/cifs-download-list/div/div[2]/div/table/tbody/tr[1]/td[5]/a[1]/i
    Input Text                          ${filename}     แก้ไข
    Clear Element Text                  ${daydownload}
    Input Text                          ${daydownload}  20
    Click Element                       ${Editdownload}
    WaitandcheckText                    วันที่มีผลจะต้องไม่น้อยกว่าวันปัจจุบัน

TC-CMSCIR-2001-05     
    login valid
    Click Element                       ${download}
    WaitandcheckText                    ดาวน์โหลด
    ${gettext}       Get Text           xpath:/html/body/cifs-root/div/cifs-download/div/div[2]/cifs-download-list/div/div[2]/div/table/tbody/tr[7]/td[2]
    Click Element                       xpath:/html/body/cifs-root/div/cifs-download/div/div[2]/cifs-download-list/div/div[2]/div/table/tbody/tr[7]/td[5]/a[2]/i
    Click Element                       xpath:/html/body/div/div/div[3]/button[1]       
    Wait Until Page Does Not Contain    ${gettext}

# TC-CMSCIR-2001-06 ยังไม่เสร็จ

TC-CMSCIR-3001-01
    login valid
