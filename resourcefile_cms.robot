*** Variables ***
# Link&Browser
${url_cms}          http://localhost:4300/#/

# locater
    #login page
@{email}                pantest003@gmail.com    cir.policepolice@gmail.com      # First=valid   second=invalid
${email_locater}        xpath:/html/body/cifs-root/div/cifs-account-login-form/section/div/div/div/form/div[1]/div/input
@{password}             pandapanda123           password                        # First=valid   second=invalid
${password_locater}     xpath:/html/body/cifs-root/div/cifs-account-login-form/section/div/div/div/form/div[2]/div/div/div/input
${login_button}         xpath:/html/body/cifs-root/div/cifs-account-login-form/section/div/div/div/form/div[3]/button
${forgot_password}      xpath:/html/body/cifs-root/div/cifs-account-login-form/section/div/div/div/div[1]/div/a
    #forgot password page
${emailF_locator}       xpath:/html/body/cifs-root/div/cifs-account-forgot-password/div/div/section/div/div/div/form/div[1]/div/input
${forgot_button}        xpath:/html/body/cifs-root/div/cifs-account-forgot-password/div/div/section/div/div/div/form/div[3]/button
${input_OTP}            xpath:/html/body/cifs-root/div/cifs-account-forgot-password-auth/div/div/section/div/div/div/form/div/div[2]/input
${verify_OTP}           xpath:/html/body/cifs-root/div/cifs-account-forgot-password-auth/div/div/section/div/div/div/div/button
${newpassword}          xpath:/html/body/cifs-root/div/cifs-account-reset-password/div/div/section/div/div/div/form/div[1]/div[1]/div/input
${newpassword2}         xpath:/html/body/cifs-root/div/cifs-account-reset-password/div/div/section/div/div/div/form/div[2]/div/div/input
${comfirmnewpassword}   xpath:/html/body/cifs-root/div/cifs-account-reset-password/div/div/section/div/div/div/form/div[3]/div[2]/button
    #MenuBar
${home}                 xpath:/html/body/cifs-root/div/cifs-home/div/div[1]/cifs-navbar/div/div/nav/ul/li[2]/a
${cover}                xpath:/html/body/cifs-root/div/cifs-home/div/div[1]/cifs-navbar/div/div/nav/ul/li[3]/a
${about}                xpath:/html/body/cifs-root/div/cifs-home/div/div[1]/cifs-navbar/div/div/nav/ul/li[4]/a
${news}                 xpath:/html/body/cifs-root/div/cifs-home/div/div[1]/cifs-navbar/div/div/nav/ul/li[5]/a
${article}              xpath:/html/body/cifs-root/div/cifs-home/div/div[1]/cifs-navbar/div/div/nav/ul/li[6]/a
${download}             xpath:/html/body/cifs-root/div/cifs-home/div/div[1]/cifs-navbar/div/div/nav/ul/li[7]/a
${faq}                  xpath:/html/body/cifs-root/div/cifs-home/div/div[1]/cifs-navbar/div/div/nav/ul/li[8]/a
${contactus}            xpath:/html/body/cifs-root/div/cifs-home/div/div[1]/cifs-navbar/div/div/nav/ul/li[9]/a
${log}                  xpath:/html/body/cifs-root/div/cifs-home/div/div[1]/cifs-navbar/div/div/nav/ul/li[10]/a
${logout}               xpath:/html/body/cifs-root/div/cifs-home/div/div[1]/cifs-navbar/div/div/nav/ul/li[11]/a
    #DownloadPage
${downloadAdd}          xpath:/html/body/cifs-root/div/cifs-download/div/div[2]/cifs-download-list/div/div[1]/div[2]/button
${uploadfile}           xpath://*[@id="file"]
${filename}             xpath://*[@id="fileName"]
${Pyblish}              xpath://*[@id="gridCheck1"]
${Addnewdownload}       xpath:/html/body/cifs-root/div/cifs-download/div/div[2]/cifs-download-add/form/div/div[2]/div[3]/div/button[2]
${Editdownload}         xpath:/html/body/cifs-root/div/cifs-download/div/div[2]/cifs-download-edit/form/div/div[2]/div[3]/div/button[2]
${daydownload}          xpath://*[@id="day"]
*** Keywords ***
Input Credentials    
    [Arguments]         ${email}                    ${password}    
    Input Text          ${email_locater}            ${email}
    Input Text          ${password_locater}         ${password}

login valid 
    openbrowserandcheckpage
    Input Credentials   @{email}[0]         @{password}[0]
    Click Element       ${login_button}
    WaitandcheckText    หน้าหลัก

openbrowserandcheckpage
    Implicit Wait
    open Browser        ${url_cms}  @{browser}[0]
    WaitandcheckText    เข้าสู่ระบบ
    WaitandcheckText    ระบบจัดการเว็บไซต์

Email Verification
    Open Mailbox        host=imap.gmail.com:993    
    ...                 user=cir.policepolice@gmail.com
    ...                 password=policepolice
    ${LATEST} =         Wait For Email    sender=cir.cifs@cifs.mail.go.th    timeout=300     
    ${HTML} =           Open Link From Email    ${LATEST}
    Should Contain      ${HTML}    Your email address has been updated
    Close Mailbox