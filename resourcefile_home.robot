*** Variables ***

# Link&Browser
${url}                      http://192.168.36.111/cir/#/    
@{browser}                  chrome  firefox
${Clicktowebsite}           xpath://*[@id="announcement-pointer"]
# On MenuBar
@{Onmenubar}                id:header-faq
...                         id:header-contact
...                         id:header-download

# MenuBar
@{menubar}                  id:header-nav-home     
...                         id:header-nav-about
...                         id:header-nav-missing 
...                         id:header-nav-anonymous-body
...                         id:header-nav-stat     
...                         id:header-nav-service-process

# SubMenu For About
@{submenuabout}             id:header-nav-about-prime    
...                         id:header-nav-about-structure    
...                         id:header-nav-about-proficient
...                         id:header-nav-about-role    
...                         id:header-nav-about-cifs    
...                         id:header-nav-about-committee
...                         id:header-nav-about-rule

# SubMenu For Submenu About
@{submenuforsubmenuabout}   id:header-nav-about-rule-missing
...                         id:header-nav-about-rule-anonymous
...                         id:header-nav-about-unknown

# SubMenu For Missing
@{submenumissing}           id:header-nav-missing-search
...                         id:header-nav-missing-announcement

# SubMenu For anonymous
${serviceanonynousbody}     id:header-nav-anonymous-body-service-unknown

# Main Page
${forofficer}           id:banner-officer-service
${Humanservice}         id:banner-user-service
${Unknowbody}           id:home-announcement-unknown-body
${Reportmissing}        id:home-announcement-reportMissingPerson
${Searchmissing}        id:home-announcement-searchMissingPerson
${Moreinformation}      id:home-announcement-service
${ViewStat}             id:home-stat-detail
${NewReport}            id:home-news-news
${ViewNew}              id:home-new-viewAll
${NewArticle}           id:home-news-article
${ViewArticle}          id:home-acticle-viewAll
@{AlliesList}           id:home-alies-ago               id:home-alies-forensic
...                     id:home-alies-dopa              id:home-alies-ifm
...                     id:home-alies-dsdw              id:home-alies-m-society
...                     id:home-alies-moi               id:home-alies-moj
...                     id:home-alies-moph              id:home-alies-oja
...                     id:home-alies-royalthaipolice   id:home-alies-mpic

# Footer
@{FooterCenter}         id:shared-footer-news           id:shared-foot4er-article
...                     id:shared-foot4er-download      id:shared-foot4er-rating
...                     id:shared-foot4er-faq           id:shared-footer-statistic        
@{FooterRightUp}        id:shared-footer-service-process            id:shared-footer-missing    id:shared-footer-service-missing
@{FooterRightDown}      id:shared-footer-about-law      id:shared-footer-about-cifs-structure
...                     id:shared-footer-about-board-proficienf     id:shared-footer-about-cifs     

# SearchMissing
${MissingFirstname}         id:missing-firstname
${MissingLastname}          id:missing-lasttname
${ClickMissingnationality}  xpath://*[@class='ng-input']
${ChooseThainationality}   xpath://*[@class='ng-select-dropdown-outer bottom']//div[15]
${Sex}                      id:inputState
${startDay}                 id:missing-startDay
${startMonth}               id:missing-startMonth
${startyear}                id:missing-startYear
${endDay}                   id:missing-endDay
${endMonth}                 id:missing-endMounth
@{submitORclear}            id:missing-search
...                         id:missing-clearData
${reportMissingPerson}      id:missing-reportMissingPerson
${Firstrow}                 //button[@class='btn btn-outline-info']
*** Keywords ***
GoToWeb
    Open Browser        ${url}    @{browser}[0]    
    Click Element       ${Clicktowebsite}

UnknowBodyDetail
    Wait Until Page Contains        ข้อมูลเบื้องต้น   
    Page Should Contain             ข้อมูลเบื้องต้น
    Page Should Contain             เลขทะเบียน ค.พ.ศ. 
    Page Should Contain             ชื่อ - นามสกุล
    Page Should Contain             ต่างชาติ
    Page Should Contain             วันที่ชันสูตร
    Page Should Contain             อายุ (โดยประมาณ)
    Page Should Contain             เพศ
CheckUnknowBodyDetail
    Wait Until Element Is Visible   xpath:/html/body/cifs-app/cifs-service/cifs-service-unknown/div/div[1]/div[1]/cifs-profile-unknown-item/div/div[1]
    Click Element                   xpath:/html/body/cifs-app/cifs-service/cifs-service-unknown/div/div[1]/div[1]/cifs-profile-unknown-item/div/div[1]
    Click Element                   xpath://div[@class='curtain hide']
    UnknowBodyDetail
    Go Back                         
    Wait Until Element Is Visible   xpath:/html/body/cifs-app/cifs-service/cifs-service-unknown/div/div[1]/div[1]/cifs-profile-unknown-item/div/div[1]
    Click Element                   xpath://div[2]//cifs-profile-unknown-item[1]//div[1]//div[2]//div[1]

MissingDetail
    Wait Until Page Contains        ข้อมูลเบื้องต้น
    Page Should Contain             เลขทะเบียน ค.พ.ศ.
    Page Should Contain             ชื่อ - นามสกุล
    Page Should Contain             สัญชาติ
    Page Should Contain             วันที่สูญหาย
    Page Should Contain             เวลาที่สูญหาย
    Page Should Contain             อายุวันที่สูญหาย
    Page Should Contain             อายุ (ณ วันที่ปัจจุบัน)
    Page Should Contain             เพศ
CheckMissingDetail
    Wait Until Element Is Visible   //div[@class='row mx-0']//div[1]//cifs-profile-item[1]//div[1]//img[1]
    Click Element                   //div[@class='row mx-0']//div[1]//cifs-profile-item[1]//div[1]//img[1]
    MissingDetail

textsearchmissing
    Wait Until Element Is Visible       ${MissingFirstname}
    Input Text                          ${MissingFirstname}    ดอกไม้
    Input Text                          ${MissingLastname}     สีเหลือง
    Click Element                       ${ClickMissingnationality}
    Click Element                       ${ChooseThainationality}
    Select From List by Value           ${Sex}          1
    Select From List by Value           ${startDay}     5
    Select From List by Value           ${startMonth}   3 
    Input Text                          ${startyear}            2562
    Select From List by Value           ${endDay}       30
    Select From List by Value           ${endMonth}     12

Implicit Wait
    Set Selenium Implicit Wait      30 seconds

TextandClickSearch
    textsearchmissing
    Click Element                   @{submitORclear}[1]
    textsearchmissing
    Click Element                   @{submitORclear}[0]

WaitandcheckText
    [Arguments]                     ${text}
    Wait Until Page Contains        ${text}
    Page Should Contain             ${text}
    
    